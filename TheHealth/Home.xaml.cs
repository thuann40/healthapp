﻿using System;
using System.Collections.Generic;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;

namespace TheHealth
{
    public partial class Home : ContentPage
    {
        public Home() {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected override void OnAppearing() {
            base.OnAppearing();
            var entries = new[] {
                new ChartEntry(47)
             {
                 Color = SKColor.Parse("#089BAB")
             },
             new ChartEntry(53)
             {
                 Color = SKColor.Parse("#EBEFF0")
             },

            };
            chartView.Chart = new DonutChart() {

                Entries = entries,
                HoleRadius = 0.9f,
            };
            listPracticeCollection.ItemsSource = new List<Practice>() {
                new Practice() {
                    BackgroundColor=Color.White,
                    Source="Shoes",
                },new Practice() {
                    Source="dumbbell",
                },new Practice() {
                    Source="Bike",
                },new Practice() {
                    Source="Apple",
                },
            };
            
        }

        void OnPaintSurface(System.Object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs e) {

        }

        void ImageButton_Clicked(System.Object sender, System.EventArgs e) {
            Navigation.PushAsync(new StayFit());
        }
    }
    class Practice
    {
        public Color BackgroundColor { get; set; } = Color.FromHex("#089BAB");
        public string Source { get; set; }
    }
}
