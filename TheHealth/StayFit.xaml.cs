﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TheHealth
{
    public partial class StayFit : ContentPage
    {
        public StayFit() {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

        }
        protected override void OnAppearing() {
            var listDay = new List<DayOfWeeks>() {
                    new DayOfWeeks {
                    Day = "Mon",
                },
                    new DayOfWeeks {
                    Day = "Tus",
                },
                    new DayOfWeeks {
                    Day = "Wed",
                },
                    new DayOfWeeks {
                    Day = "Thus",
                },
                    new DayOfWeeks {
                    Day = "Fri",
                    OpacityDay=1,
                    
                },
                    new DayOfWeeks {
                    Day = "Sat",
                },
                    new DayOfWeeks {
                    Day = "Sun",
                },
            };
            DateTimeCollectionView.ItemsSource = listDay;
            base.OnAppearing();
        }

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e) {
            Navigation.PopAsync();
        }
    }
    class DayOfWeeks
    {
        public string Day { get; set; }
        public double OpacityDay { get; set; } = 0.4;
    }
}
